Source: python-django-celery-results
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Michael Fladischer <fladi@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 pybuild-plugin-pyproject,
 python3-all,
 python3-celery,
 python3-django,
 python3-psycopg,
 python3-pytest,
 python3-pytest-benchmark,
 python3-pytest-django,
 python3-setuptools,
 python3-sphinx,
 python3-sphinx-celery,
Standards-Version: 4.6.2
Homepage: https://github.com/celery/django-celery-results/
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-django-celery-results
Vcs-Git: https://salsa.debian.org/python-team/packages/python-django-celery-results.git
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no
X-Style: black

Package: python-django-celery-results-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Multi-Arch: foreign
Description: Celery result backends for Django (Documentation)
 This extension enables you to store Celery task results using the Django ORM.
 .
 It defines a single model (django_celery_results.models.TaskResult) used to
 store task results, and you can query this database table like any other Django
 model.
 .
 This package contains the documentation.

Package: python3-django-celery-results
Architecture: all
Depends:
 python3-django,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-django-celery-results-doc,
Description: Celery result backends for Django (Python3 version)
 This extension enables you to store Celery task results using the Django ORM.
 .
 It defines a single model (django_celery_results.models.TaskResult) used to
 store task results, and you can query this database table like any other Django
 model.
 .
 This package contains the Python 3 version of the library.
